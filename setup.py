from distutils.core import setup

setup(name='Redi2Casa-Py',
    version='0.1dev',
    url='www.aaaaaaa.com',
    packages=['redi2casa',],
    license='Creative Commons Attribution-Noncommercial-Share Alike license',
    long_description=open('README.txt').read(),
    author="A Sravan Kumar",
    author_email="asravan2007@gmail.com"
)
