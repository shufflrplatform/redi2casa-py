import cql


class Redi2Casa:
    def __init__(self, host, port, keyspace):
        self.cassandra_host = host
        self.cassandra_port = port
        self.cassandra_keyspace = keyspace
        self.connection = self.connect()

    def connect(self):
        return cql.connect(self.cassandra_host, self.cassandra_port, self.cassandra_keyspace, cql_version='3.0.0')

    def set(self, key, value):
        for i in range(0, 3):
            try:
                cursor = self.connection.cursor()
                cursor.execute('UPDATE keyvalue set value=:row_value where key=:row_key', dict(row_key = str(key), row_value = str(value)))
                cursor.close()
                return 1
            except Exception, ex:
                print ex
                self.connection = self.connect()

    def set_with_expire(self, key, ttl, value):
        for i in range(0, 3):
            try:
                cursor = self.connection.cursor()
                cursor.execute('UPDATE keyvalue using TTL :ttl_value set value=:row_value where key=:row_key', dict(ttl_value = ttl, row_key = str(key), row_value = str(value)))
                cursor.close()
                return 1
            except Exception, ex:
                print ex
                self.connection = self.connect()



    def get(self, key):
        value = None
        for i in range(0, 3):
            try:
                cursor = self.connection.cursor()
                cursor.execute('select * from keyvalue where key=:row_key', dict(row_key = str(key)))
                rows = cursor.fetchall()
                if not rows: return value
                value = rows[0][1]
                return value
            except:
                self.connection = self.connect()
        return value

    def hget(self, key, column_name):
        value = None
        for i in range(0, 3):
            try:
                cursor = self.connection.cursor()
                cursor.execute('select value from hashes where key=:row_key and column1=:col_name', dict(row_key = str(key), col_name = str(column_name)))
                rows = cursor.fetchall()
                if not rows: return value
                value = rows[0][0]
                return value
            except:
                self.connection = self.connect()
        return value

    def hset(self, key, column_name, column_value):
        for i in range(0, 3):
            try:
                cursor = self.connection.cursor()
                cursor.execute('UPDATE hashes SET value=:val WHERE column1=:col_name and key=:row_key', dict(val = str(column_value), row_key = str(key), col_name = str(column_name)))
            except:
                self.connection = self.connect()

    def hmset(self, key, mapping):
        for i in range(0, 3):
            try:
                for column_key in mapping:
                    cursor = self.connection.cursor()
                    cursor.execute('UPDATE hashes SET value=:val WHERE column1=:col_name and key=:row_key', dict(val = str(mapping[column_key]), row_key = str(key), col_name = str(column_key)))
            except:
                self.connection = self.connect()

    def hset_with_expire(self, key, ttl, column_name, column_value):
        for i in range(0, 3):
            try:
                cursor = self.connection.cursor()
                cursor.execute('UPDATE hashes using TTL :ttl_value SET value=:val WHERE column1=:col_name and key=:row_key', dict(val = str(column_value), row_key = str(key), col_name = str(column_name), ttl_value = ttl))
            except:
                self.connection = self.connect()

    def hmset_with_expire(self, key, ttl, mapping):
        for i in range(0, 3):
            try:
                for column_key in mapping:
                    cursor = self.connection.cursor()
                    cursor.execute('UPDATE hashes using TTL :ttl_value SET value=:val WHERE column1=:col_name and key=:row_key', dict(val = str(mapping[column_key]), row_key = str(key), col_name = str(column_key), ttl_value = ttl))
            except:
                self.connection = self.connect()

    def hgetall(self, key):
        value = {}
        for i in range(0, 3):
            try:
                cursor = self.connection.cursor()
                cursor.execute('select * from hashes where key=:row_key', dict(row_key = str(key)))
                rows = cursor.fetchall()
                if not rows: return value
                value = dict([(row[1], row[2]) for row in rows])
                return value
            except:
                self.connection = self.connect()
        return value

    def sadd(elf, key, member):
        members = [member]
        for i in range(0,3):
            try:
                cursor = self.connection.cursor()
                query = 'update sets set members = members + {0} where key = {1}'.format('{' +','.join(map(lambda x:"'" + str(x) + "'", members)) + '}', "'" + str(key) + "'")
                status = cursor.execute(query)
                if status: return status
            except Exception, ex: 
                print ex
                self.connection = self.connect()
        return

    def sadd_with_expire(self, key, ttl, member):
        members = [member]
        for i in range(0,3):
            try:
                cursor = self.connection.cursor()
                query = 'update sets using ttl {0} set members = members + {1} where key = {2}'.format(str(ttl), '{' +','.join(map(lambda x:"'" + str(x) + "'", members)) + '}', "'" + str(key) + "'")
                status = cursor.execute(query)
                if status: return status
            except Exception, ex: 
                print ex
                self.connection = self.connect()
        return

    def smembers(self, key):
        members = []
        for i in range(0,3):
            try:
                cursor = self.connection.cursor()
                cursor.execute("select * from sets where key = :row_key", dict(row_key = str(key)))
                rows = cursor.fetchall()
                if not rows: return members
                members = list(rows[0][1])
                return set(members)
            except Exception, ex: 
                print ex
                self.connection = self.connect()
        return set(members)

    def setex(self, key, ttl, value):
        status = False
        for i in range(0,3):
            try:
                cursor = self.connection.cursor()
                status = cursor.execute("UPDATE keyvalue using TTL :ttl_value set value = :row_value where key = :row_key", dict(row_key = str(key), ttl_value = ttl, row_value = str(value)))
                if status: return status
            except Exception, ex: 
                print ex
                self.connection = self.connect()
        return status

    def hincrby(self, key, column, value):
        status = False
        for i in range(0,3):
            try:
                cursor = self.connection.cursor()
                status = cursor.execute("UPDATE counters set value = value + :incr_value where key = :row_key and column1 = :column_value", dict(row_key = str(key), incr_value = str(value), column_value = column))
                if status: return status
            except Exception, ex: 
                self.connection = self.connect()
        return status
 
    def incr(self, key):
        self.hincrby(key, '@', 1)

    def delete(self, key, key_type):
        status = False
        for i in range(0, 3): 
            try:
                cursor = self.connection.cursor()
                if key_type == "keyvalue":
                    status = cursor.execute("delete from keyvalue where key = :row_key", dict(row_key = str(key)))
                elif key_type == "hash":
                    status = cursor.execute("delete from hashes where key = :row_key", dict(row_key = str(key)))
                elif key_type == "list":
                    status = cursor.execute("delete from lists where key = :row_key", dict(row_key = str(key)))
                elif key_type == "set":
                    status = cursor.execute("delete from sets where key = :row_key", dict(row_key = str(key)))
                elif key_type == "sorted_set":
                    status = cursor.execute("delete from sorted_sets where key = :row_key", dict(row_key = str(key)))
                elif status: return status
            except Exception, ex: 
                self.connection = self.connect()
        return status
